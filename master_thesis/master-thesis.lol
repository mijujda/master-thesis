\contentsline {lstlisting}{\numberline {2.1}{\ignorespaces Textov\IeC {\'y} v\IeC {\'y}stup p\IeC {\v r}\IeC {\'\i }kazu \texttt {show vlan brief}\relax }}{8}{lstlisting.2.1}
\contentsline {lstlisting}{\numberline {2.2}{\ignorespaces Reprezentace p\IeC {\v r}\IeC {\'\i }kazu \texttt {show vlan brief} ve form\IeC {\'a}tu JSON\relax }}{8}{lstlisting.2.2}
\contentsline {lstlisting}{\numberline {2.3}{\ignorespaces Uk\IeC {\'a}zka NETCONF zpr\IeC {\'a}vy server hello\relax }}{12}{lstlisting.2.3}
\contentsline {lstlisting}{\numberline {4.1}{\ignorespaces Z\IeC {\'a}kladn\IeC {\'\i } pou\IeC {\v z}it\IeC {\'\i } knihovny requests\relax }}{27}{lstlisting.4.1}
\contentsline {lstlisting}{\numberline {4.2}{\ignorespaces Inicialzace spojen\IeC {\'\i } s APIC-EM REST API \relax }}{28}{lstlisting.4.2}
\contentsline {lstlisting}{\numberline {4.3}{\ignorespaces \IeC {\'U}prava rozm\IeC {\v e}r\IeC {\r u} virtu\IeC {\'a}ln\IeC {\'\i }ho termin\IeC {\'a}lu\relax }}{30}{lstlisting.4.3}
\contentsline {lstlisting}{\numberline {4.4}{\ignorespaces Inicialzace spojen\IeC {\'\i } se za\IeC {\v r}\IeC {\'\i }zen\IeC {\'\i }m prost\IeC {\v r}ednictv\IeC {\'\i }m CLI \relax }}{31}{lstlisting.4.4}
\contentsline {lstlisting}{\numberline {4.5}{\ignorespaces Hled\IeC {\'a}n\IeC {\'\i } MAC adres pomoc\IeC {\'\i } regul\IeC {\'a}rn\IeC {\'\i }ho v\IeC {\'y}razu v Pythonu\relax }}{33}{lstlisting.4.5}
\contentsline {lstlisting}{\numberline {4.6}{\ignorespaces Reprezentace v\IeC {\'y}stupu \texttt {show mac address-table} ve form\IeC {\'a}tu JSON\relax }}{34}{lstlisting.4.6}
\contentsline {lstlisting}{\numberline {4.7}{\ignorespaces Parsov\IeC {\'a}n\IeC {\'\i } \texttt {show etherchannel summary} - 1. krok\relax }}{36}{lstlisting.4.7}
\contentsline {lstlisting}{\numberline {4.8}{\ignorespaces Parsov\IeC {\'a}n\IeC {\'\i } \texttt {show etherchannel summary}\relax }}{37}{lstlisting.4.8}
\contentsline {lstlisting}{\numberline {4.9}{\ignorespaces Z\IeC {\'\i }sk\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } strukturovan\IeC {\'y}ch dat - get\_version()\relax }}{38}{lstlisting.4.9}
\contentsline {lstlisting}{\numberline {4.10}{\ignorespaces Pou\IeC {\v z}\IeC {\'\i }t\IeC {\'\i } objektu Cisco\_IOS\_Model\relax }}{39}{lstlisting.4.10}
\contentsline {lstlisting}{\numberline {4.11}{\ignorespaces Pou\IeC {\v z}\IeC {\'\i }t\IeC {\'\i } objektu Neighbor\_Discovery\relax }}{41}{lstlisting.4.11}
\contentsline {lstlisting}{\numberline {4.12}{\ignorespaces Pou\IeC {\v z}\IeC {\'\i }t\IeC {\'\i } objektu Topology\relax }}{42}{lstlisting.4.12}
