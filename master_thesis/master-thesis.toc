\babel@toc {czech}{}
\contentsline {chapter}{Abstrakt}{iv}{chapter*.1}
\contentsline {chapter}{Seznam pou\IeC {\v z}it\IeC {\'y}ch zkratek}{vi}{section*.3}
\contentsline {chapter}{Seznam obr\IeC {\'a}zk\IeC {\r u}}{viii}{chapter*.5}
\contentsline {chapter}{Seznam uk\IeC {\'a}zek}{ix}{chapter*.6}
\contentsline {chapter}{\numberline {1}\IeC {\'U}vod}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Spr\IeC {\'a}va datov\IeC {\'y}ch s\IeC {\'\i }t\IeC {\'\i }}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}\IeC {\'U}vod do problematiky automatizace datov\IeC {\'y}ch s\IeC {\'\i }t\IeC {\'\i }}{3}{subsection.1.1.1}
\contentsline {section}{\numberline {1.2}Zam\IeC {\v e}\IeC {\v r}en\IeC {\'\i } a c\IeC {\'\i }le pr\IeC {\'a}ce}{4}{section.1.2}
\contentsline {chapter}{\numberline {2}Anal\IeC {\'y}za}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}\IeC {\v C}ast\IeC {\'e} probl\IeC {\'e}my p\IeC {\v r}i spr\IeC {\'a}v\IeC {\v e} s\IeC {\'\i }t\IeC {\'\i }}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Rozhran\IeC {\'\i } pro komunikaci}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}P\IeC {\v r}\IeC {\'\i }kazov\IeC {\'a} \IeC {\v r}\IeC {\'a}dka}{7}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}\acrlong {snmp}}{9}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}NETCONF}{9}{subsection.2.2.3}
\contentsline {subsubsection}{Modely YANG}{12}{lstlisting.2.3}
\contentsline {section}{\numberline {2.3}S\IeC {\'\i }\IeC {\v t}ov\IeC {\'e} \IeC {\v r}adi\IeC {\v c}e}{12}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}P\IeC {\v r}ehled s\IeC {\'\i }\IeC {\v t}ov\IeC {\'y}ch \IeC {\v r}adi\IeC {\v c}\IeC {\r u}}{12}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}APIC-EM}{14}{subsection.2.3.2}
\contentsline {subsubsection}{Z\IeC {\'a}kladn\IeC {\'\i } parametry APIC-EM}{15}{subsection.2.3.2}
\contentsline {subsubsection}{Aplikace APIC-EM}{17}{figure.2.2}
\contentsline {subsection}{\numberline {2.3.3}DNA Center}{18}{subsection.2.3.3}
\contentsline {chapter}{\numberline {3}N\IeC {\'a}vrh softwarov\IeC {\'e}ho n\IeC {\'a}stroje}{20}{chapter.3}
\contentsline {section}{\numberline {3.1}Z\IeC {\'a}kladn\IeC {\'\i } c\IeC {\'\i }le}{20}{section.3.1}
\contentsline {section}{\numberline {3.2}Anal\IeC {\'y}za dostupn\IeC {\'y}ch mo\IeC {\v z}nost\IeC {\'\i }}{20}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Programovac\IeC {\'\i } jazyk}{20}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Komunika\IeC {\v c}n\IeC {\'\i } rozhran\IeC {\'\i }}{21}{subsection.3.2.2}
\contentsline {chapter}{\numberline {4}N\IeC {\'a}stroj NUAAL}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}Architektura n\IeC {\'a}stroje NUAAL}{25}{section.4.1}
\contentsline {section}{\numberline {4.2}Komunika\IeC {\v c}n\IeC {\'\i } vrstva}{27}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Programov\IeC {\'y} p\IeC {\v r}\IeC {\'\i }stup k REST API}{27}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Programov\IeC {\'y} p\IeC {\v r}\IeC {\'\i }stup k CLI}{29}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Zpracov\IeC {\'a}n\IeC {\'\i } textov\IeC {\'y}ch v\IeC {\'y}stup\IeC {\r u}}{32}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Komunikace se za\IeC {\v r}\IeC {\'\i }zen\IeC {\'\i }mi}{37}{subsection.4.2.4}
\contentsline {section}{\numberline {4.3}Modelov\IeC {\'a} vrstva}{38}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}S\IeC {\'\i }\IeC {\v t}ov\IeC {\'a} topologie}{40}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Zpracov\IeC {\'a}n\IeC {\'\i } form\IeC {\'a}tovan\IeC {\'y}ch dat}{42}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Testov\IeC {\'a}n\IeC {\'\i } n\IeC {\'a}stroje}{43}{section.4.4}
\contentsline {section}{\numberline {4.5}Instalace a distribuce n\IeC {\'a}stroje}{43}{section.4.5}
\contentsline {section}{\numberline {4.6}Roz\IeC {\v s}\IeC {\'\i }\IeC {\v r}en\IeC {\'\i } n\IeC {\'a}stroje NUAAL}{44}{section.4.6}
\contentsline {chapter}{\numberline {5}Z\IeC {\'a}v\IeC {\v e}r}{46}{chapter.5}
\contentsline {section}{\numberline {5.1}P\IeC {\v r}\IeC {\'\i }nosy pr\IeC {\'a}ce}{47}{section.5.1}
\contentsline {section}{\numberline {5.2}Budouc\IeC {\'\i } roz\IeC {\v s}\IeC {\'\i }\IeC {\v r}en\IeC {\'\i }}{47}{section.5.2}
\contentsline {chapter}{Seznam pou\IeC {\v z}it\IeC {\'y}ch zdroj\IeC {\r u}}{v}{chapter*.8}
