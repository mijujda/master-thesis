\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{master-thesis}[2015/02/16 v0.1 (Ing. Milos Kozak)]

\newif\if@en \@enfalse
\DeclareOption{en}{\@entrue}


\ProcessOptions \relax

\LoadClass[oneside,a4paper,11pt]{book}
\RequirePackage[margin=2.5cm,left=2.5cm]{geometry}


\RequirePackage{graphicx}
\RequirePackage[utf8]{inputenc}
\RequirePackage{fancyhdr}
\setlength{\headheight}{10pt}


\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt} % optional
\fancyhead[L]{\leftmark}

\cfoot{\thepage}




% Informace o skole - mozno menit dynamicky skrze optiony
\if@en
	\newcommand\University{Czech Technical University in Prague}
	\newcommand\Faculty{Faculty of Electrical Engineering}
	\newcommand\Department{Department of Telecommunication Engineering}
	\newcommand\projectName{Project report}
	\def\supervisorName{Supervisor}
	\def\courseNameTitle{Course name}
	\def\studyProgramTitle{Study program}
	\def\specializationTitle{Specialization}
	\def\courseCodeTitle{Course code}
	\RequirePackage[english]{babel}
\else
	\RequirePackage[czech]{babel}	
	\newcommand\University{\v{C}esk\'{e} vysok\'{e} u\v{c}en\'{\i} technick\'{e} v Praze}
	\newcommand\Faculty{Fakulta elektrotechnick\'{a}}
	\newcommand\Department{Katedra telekomunika\v{c}n\'{\i} techniky}
	\newcommand\projectName{Diplomová práce}
	\def\supervisorName{Vedouc\'{\i} pr\'{a}ce}	
	\def\studyProgramTitle{Studijní program}
	\def\specializationTitle{Studijní obor}
\fi



\def \@supervisor{Unknown}
\newcommand{\supervisor}[1]{ \def \@supervisor{#1} }


\def\@studyProgram{studyProgram}
\def\@specialization{specialization}
\newcommand{\courseCode}[1]{ \def \@courseCode{#1} }
\newcommand{\courseName}[1]{ \def \@courseName{#1} }
\newcommand{\studyProgram}[1]{ \def \@studyProgram{#1} }
\newcommand{\specialization}[1]{ \def \@specialization{#1} }


\renewcommand\maketitle{\par
	% Volba stylu uvodni stranky
	\@maketitle

	\global\let\thanks\relax
	\global\let\maketitle\relax
	\global\let\@maketitle\relax
	\global\let\@thanks\@empty
	\global\let\@author\@empty
	\global\let\@date\@empty
	\global\let\@title\@empty
	\global\let\title\relax
	\global\let\author\relax
	\global\let\date\relax
	\global\let\and\relax
}

\def\@maketitle{%

	\begin{center}%
		\large\sffamily
			\University\\ \vglue 2mm
			\Faculty\\ \vglue 2mm
			\Department\\
		
		\vglue 20mm
		\includegraphics[width=40mm]{images/LogoCVUT}
		\vglue 30mm
		{\huge\sffamily \projectName } \\
		\vglue 5mm
		{\LARGE\bfseries\@title}\\
		\vglue 10mm	
		{\large\bfseries{\@author}}\\
		\vglue 50mm
		{\large 
			
			\begin{tabular}{rl}
				\textbf{\studyProgramTitle}: & \@studyProgram \\[2mm]
				\textbf{\specializationTitle}: & \@specialization \\[2mm]
				 \textbf{\supervisorName}: & \@supervisor
			\end{tabular}
		} 
		\vglue 20mm
		\@date
		
		
	\end{center}%
	\thispagestyle{empty}
	% Strana 2
	\newpage
}


\RequirePackage{xcolor}
\RequirePackage[pdftex]{hyperref}
\hypersetup{
	colorlinks   = true,
	citecolor    = blue,
	linkcolor	 = blue,
	urlcolor	 = blue,
}

% Zapsani metadat do PDF souboru
\makeatletter
\AtBeginDocument{
	\hypersetup{
		pdftitle = {\projectName: \@title},
		pdfauthor = {\@author},
	}
}
\makeatother